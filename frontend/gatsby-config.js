/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: "SM Bud",
    titleTemplate: `React WebApp`,
    description: ``,
    author: `DSIDE`,
    twitterUsername: ``,
    image: 'bg-image-1.jpg',
    siteUrl: 'http://smbud.eu/',
    form_url: "http://smbud.eu/api/ru/home/addOrder/",
    contact: {
      postal_code: 'ul. Al. "Solidarności" 155/13,<br/> 00-877 Warszawa',
      address: 'Mykola Mostovyi <br/> tel. +48 570 901 091 <br/>mail: biuro@smbudspzoo.pl',
      email: 'biuro@smbudspzoo.pl',
      company_email: 'biuro@smbudspzoo.pl',
      company_address: 'ul. Al. "Solidarności" 155/13, 00-877 Warszawa',
      phone: '+48 570 901 091',
      phone2: '',
      social: {
        facebook: 'https://www.facebook.com/remontmieszkan.warszawapl',
        twitter: 'https://instagram.com/remontmieszkan.warszawa.pl',
        linkedin: 'https://www.linkedin.com/in/sm-bud-sp-z-o-o-11a6461b1',
        youtube: 'https://www.youtube.com/user/nmostovoy25'
      }
    },
    copyright: "SM Bud sp. z o.o. 2020. All Rights Reserved"
  },
  plugins: [
    `gatsby-plugin-styled-components`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-json`,
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `SMbud`,
        short_name: `SMbud`,
        start_url: `/`,
        background_color: `#333333`,
        theme_color: `#001c43`,
        display: `standalone`,
        "icons": [
          {
            "src": "/icons/icon-72x72.png",
            "sizes": "72x72",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-96x96.png",
            "sizes": "96x96",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-128x128.png",
            "sizes": "128x128",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-144x144.png",
            "sizes": "144x144",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-152x152.png",
            "sizes": "152x152",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-192x192.png",
            "sizes": "192x192",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-384x384.png",
            "sizes": "384x384",
            "type": "image/png"
          },
          {
            "src": "/icons/icon-512x512.png",
            "sizes": "512x512",
            "type": "image/png"
          }
        ]
      },
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'http://thern.rainbowit.net/',
        sitemap: 'http://thern.rainbowit.net/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1920
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Montserrat`, `300, 300i, 400, 400i, 500, 600, 700, 900`]
          }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/assets/img/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // The property ID; the tracking code won't be generated without it
        trackingId: "UA-158680515-1",
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: false,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Avoids sending pageview hits from custom paths
        exclude: ["/preview/**", "/do-not-track/me/too/"],
        // Delays sending pageview hits on route update (in milliseconds)
        pageTransitionDelay: 0,
        // Any additional optional fields
        sampleRate: 5,
        siteSpeedSampleRate: 10,
        cookieDomain: "example.com",
      },
    },
  ]
}
