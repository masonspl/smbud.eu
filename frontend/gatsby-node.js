const path = require(`path`)
const {slugify} = require('./src/utils/utilFunctions')

exports.onCreateNode = ({node, actions}) => {
    const { createNodeField } = actions;
    if(node.internal.type === 'MarkdownRemark'){
        const slugFromTitle = slugify(node.frontmatter.title);
        createNodeField({
            node, 
            name: 'slug',
            value: slugFromTitle
        })
    }
}




exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /bad-module/,
            use: loaders.null(),
          },
        ],
      },
    })
  }
}

const axios = require('axios');
const crypto = require('crypto');

exports.sourceNodes = async ({ boundActionCreators }) => {
  const { createNode } = boundActionCreators;

  // fetch raw data from the randomuser api
  const fetchDsideBlog = () => axios.get(`https://smbud.eu/api/pl/blog/getBlogItems/News/?format=json`);
  const fetchDsidePort = () => axios.get(`https://smbud.eu/api/pl/portfolio/getPortfolioItems/?format=json`);
  // await for results
  const res = await fetchDsideBlog();
  const res2 = await fetchDsidePort();

  // map into these results and create nodes
  res.data.map((post, i) => {
    // Create your node object
    const postNode = {
      // Required fields
      id: `${i+20}`,
      parent: `__SOURCE__`,
      internal: {
        type: `DsideBlog`, // name of the graphQL query --> allRandomUser {}
        // contentDigest will be added just after
        // but it is required
      },
      children: [],

      // Other fields that you want to query with graphQl
      postid: post.id,
      slug: post.base_name,
      main_img: post.main_image,
      thumb: post.thumbnail,
      title: post.title,
      date: post.date,
      description: post.description,
      // etc...
    }

    // Get content digest of node. (Required field)
    const contentDigest = crypto
      .createHash(`md5`)
      .update(JSON.stringify(postNode))
      .digest(`hex`);
    // add it to userNode
    postNode.internal.contentDigest = contentDigest;

    // Create node with the gatsby createNode() API
    createNode(postNode);
  });
  res2.data.map((post, i) => {
    // Create your node object
    const postNode = {
      // Required fields
      id: `${i+30}`,
      parent: `__SOURCE__`,
      internal: {
        type: `DsidePort`, // name of the graphQL query --> allRandomUser {}
        // contentDigest will be added just after
        // but it is required
      },
      children: [],

      // Other fields that you want to query with graphQl
      postid: post.id,
      name: post.name,
      thumb: post.thumbnail,
      main_img: post.main_img,
      second_img: post.second_img,
      slug: post.CURL,
      address: post.address,
      category: post.category,
      // etc...
    }

    // Get content digest of node. (Required field)
    const contentDigest = crypto
      .createHash(`md5`)
      .update(JSON.stringify(postNode))
      .digest(`hex`);
    // add it to userNode
    postNode.internal.contentDigest = contentDigest;

    // Create node with the gatsby createNode() API
    createNode(postNode);
  });

  return;
}
exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/project/)) {
    page.matchPath = "/project/*"

    // Update the page.
    createPage(page)
  }
}
exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions;
    const singleBlog = path.resolve("./src/templates/blog-template/blog-template.js");
    const blogList = path.resolve("./src/templates/blog-list/blog-list.js");
    const portfolioList = path.resolve("./src/templates/blog-list/blog-list.js");
    const singleProject = path.resolve("./src/templates/project-template/project-template.js");

    const result = await graphql(`
        {
            allMarkdownRemark{
                edges {
                  node {
                    fields{
                        slug
                    }
                  }
                }
            }
            allDsideBlog {
    edges {
      node {
        slug
      }
    }
  }           
            allDsidePort {
                edges {
                    node {
                        slug
                    }
                   
                }
            }     
        }
    `);
    const posts = result.data.allDsideBlog.edges
    posts.forEach(({node}) => {
        createPage({
            path: '/blog/'+node.slug,
            component: singleBlog,
            context: {
                slug: node.slug
            }
        })
    })
    const postsPerPage = 4;
    const numberOfPages = Math.ceil(posts.length/postsPerPage);

    Array.from({length: numberOfPages}).forEach((_, index) => {
      const isFirstPage = index === 0;
      const currentPage = index + 1;
      if(isFirstPage) return;
      createPage({
        path: `blog/page/${currentPage}`,
        component: blogList,
        context: {
          limit: postsPerPage,
          skip: index * postsPerPage,
          currentPage,
          numberOfPages
        }
      })
    })

    result.data.allDsidePort.edges.forEach(({node, next, previous}) => {
        createPage({
            path: `project/${node.curl}`,
            component: singleProject,
            context: {
                curl: node.curl,
            }
        })
    })
}
