import React from 'react'
import PropTypes from 'prop-types'
import { DiscussionEmbed, CommentCount } from 'disqus-react';

import {graphql} from 'gatsby'
import SEO from '../../components/seo';
import Layout from '../../containers/layout/layout'
import Heading from '../../components/shared/heading'
import Text from '../../components/shared/text'
import {
    BlogDetailsWrap,
    BlogDetailsHeader,
    BlogDetailsMetaWrap,
    BlogDetailsMeta,
    BlogDetailsContent,
    CommentWrap,
    CommentFormWrap
} from './blog-template.stc'
 
function BlogTemplate({data, ...restProps}) {
    const {titleStyle, metaBoxStyle, metaHeadingStyle, metaTextStyle} = restProps;
    const post = data.dsideBlog;
    const imageData = data.dsideBlog.main_img;
    const disqusShortname = 'thern-1';
    const disqusConfig = {
        url: ('https://smbud.eu/blog/'+post.slug),
        identifier: post.id,
        title: post.title,
    }
    return (
        <Layout headerLayout={2} >
            <SEO title={post.title}/>
            <BlogDetailsWrap>
                <BlogDetailsHeader style={{ backgroundImage: `url(${'https://smbud.eu'+imageData})` }}>
                    <div className="row align-items-center">
                        <div className="col-2 offset-2">
                            <div className="rn-blog-details-meta-inner">
                                <Heading {...titleStyle}>{post.title}</Heading>
                            </div>
                        </div>
                        <div className="col-1 offset-1">
                            <BlogDetailsMetaWrap>
                                <BlogDetailsMeta {...metaBoxStyle}>
                                    <Text {...metaTextStyle}>Opublikowane:</Text>
                                    <Heading {...metaHeadingStyle}>{post.date}</Heading>
                                </BlogDetailsMeta>
                                <BlogDetailsMeta {...metaBoxStyle}>
                                    <Text {...metaTextStyle}>Autor:</Text>
                                    <Heading {...metaHeadingStyle}>SM BUD sp. z o.o.</Heading>
                                </BlogDetailsMeta>
                            </BlogDetailsMetaWrap>
                        </div>
                    </div>
                </BlogDetailsHeader>
                <BlogDetailsContent>
                   <div class="rn-blog-meta-area section-pb-xl">
    <div class="row">
        <div class="col-3 offset-2">
            <div class="rn-blog-content">
               <div dangerouslySetInnerHTML={{ __html: post.description }}></div>
            </div>
        </div>
    </div>
</div>

                </BlogDetailsContent>
                <CommentWrap>
                    <CommentFormWrap>
                        <DiscussionEmbed shortname={disqusShortname} config={disqusConfig} />
                    </CommentFormWrap>
                </CommentWrap>
            </BlogDetailsWrap>
        </Layout>
    )
}

export const query = graphql `
    query($slug: String!){
        dsideBlog(slug: { eq: $slug }){
        slug
        thumb
        description
        date(formatString: "MMMM Do, YYYY")
        main_img
        title
  }
    }
`;

BlogTemplate.propTypes = {
    metaHeadingStyle: PropTypes.object,
    metaTextStyle: PropTypes.object,
    titleStyle: PropTypes.object
}

BlogTemplate.defaultProps = {
    titleStyle: {
        as: 'h1',
        color: '#fff',
        responsive: {
            small:{
                mb: '22px'
            }
        }
    },
    metaBoxStyle: {
        mb: '30px',
        responsive: {
            small: {
                mb: 0,
                pt: '11px',
                pb: '11px'
            }
        }
    },
    metaHeadingStyle: {
        as: 'h6',
        color: '#fff',
        fontSize: '12px',
        fontweight: 700,
        letterspacing: '2px',
        mb: '12px'
    },
    metaTextStyle: {
        m: 0,
        fontSize: '12px',
        color: '#fff',
        letterspacing: '1px'
    }
}

export default BlogTemplate
