import React, {Component} from 'react';
import PropTypes from 'prop-types'
import { Controller, Scene } from 'react-scrollmagic';
import SEO from '../../components/seo';
import Layout from '../../containers/layout/layout'
import Banner from '../../containers/single-project/banner'
import Heading from '../../components/shared/heading'
import Text from '../../components/shared/text'
import SectionTitle from '../../components/shared/section-title'
import Image from '../../components/image'
import Project from '../../components/project'
import {
    ProjectDetailsWrap,
    ProjectHeaderWrap,
    ProjectHeaderMeta,
    ProjectType,
    ProjectFeatureWrap,
    FullwidthBox,
    NextProjectWrap} from './project-template.stc'
class ProjectTemplate extends Component{
    constructor (props) {
        super (props)
        this.state = {
            item: [],
        }
    }
    componentDidMount(){
        fetch(`https://smbud.eu/api/pl/portfolio/getPortfolioItemDetails/${this.props.slug}/?format=json`)
        .then(res => res.json())
        .then(json => {
                this.setState({item: json});
                console.log(this);
        });
    }
    render(){
    return (
        <Layout headerLayout={2}>
            <SEO title={this.state.item.name}/>
            <Banner image={this.state.item.main_image}/>
            <ProjectDetailsWrap>
                <ProjectHeaderWrap className="section-ptb-xl">
                    <div className="row">
                        <div className="col-1 offset-1">
                            <div className="rn-project-meta-inner">

                                <ProjectHeaderMeta>
                                        <ProjectType className="wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1000ms">
                                            <h6>Rodzaj</h6>
                                            <Text>{this.state.item.category}</Text>
                                        </ProjectType>
                                        <ProjectType className="wow fadeInLeft" data-wow-delay="200ms" data-wow-duration="1000ms">
                                            <h6>Adres</h6>
                                            <Text>{this.state.item.address}</Text>
                                        </ProjectType>
                                </ProjectHeaderMeta>
                            </div>
                        </div>
                        <div className="col-3 mobile-offset-1">
                            <div className="rn-project-content">
                               <h2>{this.state.item.name}</h2>
                                <p><img src={'https://smbud.eu'+this.state.item.second_image} alt={this.state.item.name}/></p>
                            </div>
                        </div>
                    </div>
                </ProjectHeaderWrap>
            </ProjectDetailsWrap>
        </Layout>
    )
}
}
ProjectTemplate.propTypes = {
    title: PropTypes.object,
    metaHeading: PropTypes.object,
    metaText: PropTypes.object
}

ProjectTemplate.defaultProps = {
    title: {
        color: 'primary',
        fontSize: '40px',
        lineHeight: '55px',
        responsive: {
            medium: {
                fontSize: '24px',
                lineHeight: 'initial'
            }
        }
    },
    metaHeading: {
        as: 'h6',
        color: 'primary',
        fontSize: '12px',
        fontweight: 700,
        letterspacing: '2px',
        mb: '12px',
    },
    metaText: {
        m: 0,
        fontSize: '12px',
        color: '#000000',
        letterspacing: '1px',
    },
    nextProjectStyle: {
        mt: '100px',
        responsive: {
            medium: {
                mt: '60px'
            }
        }
    }
}
export default ProjectTemplate;