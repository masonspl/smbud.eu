export function createMarkup(value) { return { __html: value }; }
;
export const UseDiscount = (discount, amount) => Math.ceil(amount / 1000 * (1000 - discount * 10));
export const normalizePrice = (value) => Math.ceil(value * 100) / 100;
export const getProductInfo = (config) => {
    const getService = (product) => config.services[product.serviceIndex];
    const getPack = (product) => getService(product).packs[product.packIndex];
    const getBasePrice = (product, discount = 0) => UseDiscount(discount, getPack(product).price) + getPack(product).modulePrice * (product.extraModules || 0);
    const getText = (product) => {
        return getService(product).name.ru + ' ' + getPack(product).name.ru + ' extra modules ' + product.extraModules;
    };
    return {
        getPack,
        getService,
        getBasePrice,
        getText
    };
};
export const service1 = {
    serviceIndex: 0,
    name: {
        pl: "Podstawowy",
    },
    moduleLang: {
        ru: 'Эскизы',
        en: 'Sketches',
        pl: 'Szkice'
    },
    description: {
        ru: 'Визуально отличает Вас от Ваших конкурентов.',
        en: 'Visually distinguishes you from your competitors.',
        pl: 'Chce odświeżyć swoje mieszkanie',
    },
    partnership: false,
    addonIndicies: [1, 2],
    addonDiscounts: {
        1: 20,
    },
    packs: [
        {
            name: {
                ru: "Standart",
                en: "Lite",
                pl: "Standart",
            },
            price: 150,
            modules: 3,
            modulePrice: 69,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Zabezpieczenie powierzchni.</li>\n' +
                    '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników, listwy przypodłogowej.</li>' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 10%).</li>' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                ru: "Standard",
                en: "Standard",
                pl: "Kompleksowy",
            },
            price: 200,
            modules: 4,
            modulePrice: 49,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Zabezpieczenie powierzchni.</li>\n' +
                    '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników, listwy przypodłogowej.</li>' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 10%).</li>' +
                    '<li class="pricing-feature">Wymiana okładzin podłogowych (panel, deska)</li>' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                ru: "Business",
                en: "Business",
                pl: "Business",
            },
            price: 350,
            modules: 5,
            modulePrice: 39,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Zabezpieczenie powierzchni.</li>\n' +
                    '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników, listwy przypodłogowej.</li>' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 10%).</li>' +
                    '<li class="pricing-feature">Wymiana okładzin podłogowych (panel, deska)</li>' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>'+
                    '<li class="pricing-feature">Remont łazienki</li>'+
                    '<li class="pricing-feature">Małe przeróbki elektryki</li>',
            }
        }
    ]
};
export const service2 = {
    serviceIndex: 1,
    name: {
        pl: "Standartowy",
    },
    moduleLang: {
        ru: 'Модули',
        en: 'Modules',
        pl: 'Moduły'
    },
    description: {
        ru: 'Идеален для продажи одной услуги или продукта.',
        en: 'Best for selling a specific service or product.',
        pl: 'Planuje generalny remont mieszkania',
    },
    addonIndicies: [0],
    addonDiscounts: {
        0: 30,
    },
    packs: [
        {
            name: {
                pl: "Ekonom",
            },
            price: 400,
            modules: 3,
            modulePrice: 69,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                    '<li class="pricing-feature">Małe przeróbki elektryki</li>\n' +
                    '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                    '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 20%)</li>\n' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze (bez położenia gładzi).</li>\n' +
                    '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco)</li>\n' +
                    '<li class="pricing-feature">Montaż drzwi</li>\n' +
                    '<li class="pricing-feature">Remont łazienki (płytki do h 2.1)</li>\n' +
                    '<li class="pricing-feature">Biały montaż</li>\n' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                pl: "Standart",
            },
            price: 450,
            modules: 4,
            modulePrice: 49,
            featureDescriptions: {

                 pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                     '<li class="pricing-feature">Przeróbki elektryki</li>\n' +
                     '<li class="pricing-feature">LEDy</li>\n' +
                     '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                     '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                     '<li class="pricing-feature">Odpływ liniowy lub obmurowanie wanny</li>\n' +
                     '<li class="pricing-feature">Gipsowanie ścian</li>\n' +
                     '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>\n' +
                     '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco lub na klej)</li>\n' +
                     '<li class="pricing-feature">Montaż drzwi</li>\n' +
                     '<li class="pricing-feature">Remont łazienki</li>\n' +
                     '<li class="pricing-feature">Biały montaż</li>\n' +
                     '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                pl: "Standart +",
            },
            price: 550,
            modules: 5,
            modulePrice: 39,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                     '<li class="pricing-feature">Demontaż i postawienie ściandziałowych.</li>\n' +
                     '<li class="pricing-feature">Przeróbki elektryki</li>\n' +
                     '<li class="pricing-feature">LEDy</li>\n' +
                     '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                     '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                     '<li class="pricing-feature">Odpływ liniowy lub obmurowanie wanny</li>\n' +
                     '<li class="pricing-feature">Gipsowanie ścian</li>\n' +
                     '<li class="pricing-feature">Sufity podwieszane</li>\n' +
                     '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>\n' +
                     '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco lub na klej)</li>\n' +
                     '<li class="pricing-feature">Montaż drzwi</li>\n' +
                     '<li class="pricing-feature">Remont łazienki</li>\n' +
                     '<li class="pricing-feature">Biały montaż</li>\n' +
                     '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                pl: "Ekonom",
            },
            price: 500,
            modules: 3,
            modulePrice: 69,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                    '<li class="pricing-feature">Małe przeróbki elektryki</li>\n' +
                    '<li class="pricing-feature">Demontaż podłóg</li>\n' +
                    '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                    '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 20%)</li>\n' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze (bez położenia gładzi).</li>\n' +
                    '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco)</li>\n' +
                    '<li class="pricing-feature">Montaż drzwi</li>\n' +
                    '<li class="pricing-feature">Remont łazienki (płytki do h 2.1)</li>\n' +
                    '<li class="pricing-feature">Biały montaż</li>\n' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                pl: "Standart",
            },
            price: 580,
            modules: 4,
            modulePrice: 49,
            featureDescriptions: {

                 pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                     '<li class="pricing-feature">Przeróbki elektryki</li>\n' +
                     '<li class="pricing-feature">Demontaż podłóg</li>\n' +
                     '<li class="pricing-feature">LEDy</li>\n' +
                     '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                     '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                     '<li class="pricing-feature">Odpływ liniowy lub obmurowanie wanny</li>\n' +
                     '<li class="pricing-feature">Gipsowanie ścian</li>\n' +
                     '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>\n' +
                     '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco lub na klej)</li>\n' +
                     '<li class="pricing-feature">Montaż drzwi</li>\n' +
                     '<li class="pricing-feature">Remont łazienki</li>\n' +
                     '<li class="pricing-feature">Biały montaż</li>\n' +
                     '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        },
        {
            name: {
                pl: "Standart +",
            },
            price: 680,
            modules: 5,
            modulePrice: 39,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                     '<li class="pricing-feature">Demontaż i postawienie ściandziałowych.</li>\n' +
                     '<li class="pricing-feature">Przeróbki elektryki</li>\n' +
                     '<li class="pricing-feature">Demontaż podłóg</li>\n' +
                     '<li class="pricing-feature">LEDy</li>\n' +
                     '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                     '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                     '<li class="pricing-feature">Odpływ liniowy lub obmurowanie wanny</li>\n' +
                     '<li class="pricing-feature">Gipsowanie ścian</li>\n' +
                     '<li class="pricing-feature">Sufity podwieszane</li>\n' +
                     '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>\n' +
                     '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco lub na klej)</li>\n' +
                     '<li class="pricing-feature">Montaż drzwi</li>\n' +
                     '<li class="pricing-feature">Remont łazienki</li>\n' +
                     '<li class="pricing-feature">Biały montaż</li>\n' +
                     '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        }
    ]
};
export const service3 = {
    serviceIndex: 2,
    name: {
        pl: "Premium",
    },
    addonIndicies: [],
    addonDiscounts: {},
    moduleLang: {
        ru: 'Кампании',
        en: 'Кампании',
        pl: 'Кампании'
    },
    description: {
        ru: 'Необходимый инструмент для выхода компаний в интернет.',
        en: 'An essential tool for business on the Interent.',
        pl: 'Wszystko ma być w najwyższym standarcie',
    },
    packs: [
        {
            name: {
                ru: "Lite",
                en: "Lite",
                pl: "Kompleksowe wykończenie mieszkania",
            },
            price: 750,
            modules: 3,
            modulePrice: 69,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                     '<li class="pricing-feature">Demontaż i postawienie ściandziałowych.</li>\n' +
                     '<li class="pricing-feature">Przeróbki elektryki</li>\n' +
                     '<li class="pricing-feature">Demontaż podłóg</li>\n' +
                     '<li class="pricing-feature">LEDy</li>\n' +
                     '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                     '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                     '<li class="pricing-feature">Odpływ liniowy lub obmurowanie wanny</li>\n' +
                     '<li class="pricing-feature">Gipsowanie ścian</li>\n' +
                     '<li class="pricing-feature">Sufity podwieszane</li>\n' +
                     '<li class="pricing-feature">Malowanie na biało lub w kolorze.</li>\n' +
                     '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco lub na klej)</li>\n' +
                     '<li class="pricing-feature">Montaż drzwi</li>\n' +
                     '<li class="pricing-feature">Remont łazienki</li>\n' +
                     '<li class="pricing-feature">Biały montaż</li>\n' +
                     '<li class="pricing-feature">Utylizacja śmieć</li>',
            }
        }
    ]
};

export const service4 = {
    serviceIndex: 2,
    name: {
        pl: "Biznes",
    },
    addonIndicies: [],
    addonDiscounts: {},
    moduleLang: {
        ru: 'Кампании',
        en: 'Кампании',
        pl: 'Кампании'
    },
    description: {
        ru: 'Решение для продаж широкого ассортимента товаров.',
        en: 'The best solution for selling a wide range of products.',
        pl: 'Szukam wykonawcy do remontu biura',
    },
    packs: [
        {
            name: {
                ru: "Lite",
                en: "Lite",
                pl: "Ekonom",
            },
            price: 400,
            modules: 3,
            modulePrice: 69,
            featureDescriptions: {


                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                '<li class="pricing-feature">Małe przeróbki elektryki</li>\n' +
                '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                '<li class="pricing-feature">Drobne naprawy malarskie (do 20%)</li>\n' +
                '<li class="pricing-feature">Malowanie na biało lub w kolorze (bez położenia gładzi).</li>\n' +
                '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco)</li>\n' +
                '<li class="pricing-feature">Montaż drzwi</li>\n' +
                '<li class="pricing-feature">Remont łazienki (płytki do h 2.1)</li>\n' +
                '<li class="pricing-feature">Biały montaż</li>\n' +
                '<li class="pricing-feature">Utylizacja śmieć</li>\n',
            }
        },
        {
            name: {
                ru: "Standard",
                en: "Standard",
                pl: "Standart",
            },
            price: 450,
            modules: 4,
            modulePrice: 49,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                    '<li class="pricing-feature">Małe przeróbki elektryki</li>\n' +
                    '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                    '<li class="pricing-feature">LEDy</li>\n' +
                    '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 20%)</li>\n' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze (bez położenia gładzi).</li>\n' +
                    '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco)</li>\n' +
                    '<li class="pricing-feature">Montaż drzwi</li>\n' +
                    '<li class="pricing-feature">Remont łazienki</li>\n' +
                    '<li class="pricing-feature">Biały montaż</li>\n' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>\n',
            }
        },
        {
            name: {
                ru: "Business",
                en: "Business",
                pl: "Standart +",
            },
            price: 550,
            modules: 5,
            modulePrice: 39,
            featureDescriptions: {
                pl: '<li class="pricing-feature">Demontaż-montaż: osprzętu elektrycznego, grzejników.</li>\n' +
                    '<li class="pricing-feature">Małe przeróbki elektryki</li>\n' +
                    '<li class="pricing-feature">Demontaż i postawienie ścian działowych.</li>\n' +
                    '<li class="pricing-feature">Przeróbki Wod-Kan i CO</li>\n' +
                    '<li class="pricing-feature">LEDy</li>\n' +
                    '<li class="pricing-feature">Montaż stelaża WC</li>\n' +
                    '<li class="pricing-feature">Drobne naprawy malarskie (do 20%)</li>\n' +
                    '<li class="pricing-feature">Malowanie na biało lub w kolorze (bez położenia gładzi).</li>\n' +
                    '<li class="pricing-feature">Ulożenie okładzin podłogowych (panel lub deska - plywająco)</li>\n' +
                    '<li class="pricing-feature">Sufity podwieszane</li>\n' +
                    '<li class="pricing-feature">Montaż drzwi</li>\n' +
                    '<li class="pricing-feature">Remont łazienki</li>\n' +
                    '<li class="pricing-feature">Biały montaż</li>\n' +
                    '<li class="pricing-feature">Utylizacja śmieć</li>\n',
            }
        }
    ]
};
const pricesConfig = {
    services: [
        service1,
        service2,
        service3,
        service4,
    ]
};
export default pricesConfig;
