import React, { Fragment } from 'react';
import pricesConfig, { getProductInfo, normalizePrice } from './config';
import ViewStack from './ViewStack';
import Step1 from './Step1';
import Step3 from './Step3';
import Step4 from './Step4';
import useLang, { getLang } from '../../hooks/useLang';
import useMergeState from '../../hooks/useMergeState';
import { tail } from 'ramda';
const getDefaultOrder = () => ({
    products: [
        {
            extraModules: 0,
            packIndex: 0,
            serviceIndex: 0,
        }
    ]
});
export default () => {
    const [order, setOrder] = useMergeState(getDefaultOrder());
    const [step, setStep] = React.useState(0);
    const info = getProductInfo(pricesConfig);
    const updateOrder = (value) => {
        console.log('updateOrder', value);
        setOrder(value);
    };
    const onStep1 = (updatedOrder) => {
        updateOrder(updatedOrder);

        setTimeout(function() {setStep(1);}, 1);
    };
    const onStep2 = (updatedOrder) => {
        updateOrder(updatedOrder);
        window.scrollTo(0,0);
        setStep(2);
    };
    const onStep3 = (updatedOrder) => {
        updateOrder(updatedOrder);
        window.scrollTo(0,0);
        setStep(3);
        fetch('https://smbud.eu/api/' + getLang() + '/home/addOrder/', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify({
                name: updatedOrder.name,
                phone: updatedOrder.phone,
                totalprice: '$' + String(updatedOrder.totalPrice || '0') +
                    (updatedOrder.bill === 'periodic'
                        ? '($' + normalizePrice((updatedOrder.totalPrice / updatedOrder.term)) + ' per month)'
                        : updatedOrder.bill === 'check'
                            ? '($' + normalizePrice(updatedOrder.totalPrice / updatedOrder.count) + ' per check)'
                            : ''),
                bill: (updatedOrder.bill === 'check' ? String(updatedOrder.count || '0') : '0') + ' ',
                term: (updatedOrder.bill === 'periodic' ? String(updatedOrder.term || '0') : '0') + ' mon',
                pack: info.getText(updatedOrder.products[0]),
                addons: tail(updatedOrder.products).map(p => info.getText(p)).join(' , ') || 'No addons ',
                paymenttype: updatedOrder.bill === 'once' ? 'One time payment' : (updatedOrder.bill === 'periodic' ? 'Subscribtion' : 'Partnership'),
            }),
        }).then((s) => console.log('success', s)).catch((e) => console.log(e));
        //alert(useLang('Ваш заказ успешно отправлен на обработку', 'Your order is on a way'));
        //setOrder(getDefaultOrder());
    };
    const onStep4 = (updatedOrder) => {
        updateOrder(updatedOrder);
        window.scrollTo(0,0);
        setStep(0);
    };
    return <Fragment>
        <div className="row">
            <div className="pricing__wrapper">
            <ViewStack value={step} data={[
        () => <Step1 onSubmit={onStep1} config={pricesConfig} order={order}/>,
        () => <Step3 onSubmit={onStep3} config={pricesConfig} order={order} onBack={() => setStep(0)}/>,
        () => <Step4 onSubmit={onStep4} config={pricesConfig} order={order} onBack={() => setStep(0)}/>,
    ]}/>

            </div>
            <h5 className="lite steps">{useLang('Шаг', 'Step', 'Krok')} {step + 1}/2</h5>
            </div>

    </Fragment>;
};
