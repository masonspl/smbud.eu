import React from 'react';
import { createMarkup, UseDiscount } from './config';
import useLang from '../../hooks/useLang';
import { orderLang, startsFromLang } from './Step1';
import { take, takeLast } from 'ramda';

import { ScrollTo } from "react-scroll-to";

export default ({ onSelect, config, serviceIndex, isPopUp, discount = 0 }) => {
    function showDev() {
      document.getElementById('developerka').style.display = "flex";
      document.getElementById('nondeveloperka').style.display = "none";
    };
    function showNonDev() {
      document.getElementById('nondeveloperka').style.display = "flex";
      document.getElementById('developerka').style.display = "none";
    };
    const selectedService = config.services[serviceIndex];
    const packs = selectedService.packs;
   console.log(packs.length);
    let developerka = true;
    console.log(developerka);

    if(packs.length === 6){
    return (
        <div><h2>Wybierz rodzaj budynku:</h2><button className='btn-send btn-calc btn-inn-calc' onClick={showDev}>Developerka</button><button className='btn-send btn-calc btn-inn-calc' onClick={showNonDev}>Budynek</button>

        <div className='pricing pricing-palden index1' id="developerka" /*style={isPopUp ? { position: 'absolute' } : {}}*/>
            {take(3, selectedService.packs).map((pack, packIndex) => <div className={'pricing-item ' + ((packIndex === 2) ? 'pricing__item--featured' : '')}>
                        <div className={'pricing-deco' + ((packIndex === 3) ? 'elite' : '')}>
                            <div className='pricing-price'>{UseDiscount(discount, pack.price)}<span className='pricing-currency'> PLN</span><span className='pricing-period'>{useLang(startsFromLang)}</span>
                            </div>
                              <ScrollTo>
        {({ scroll }) => (
          <a onClick={() => { onSelect(packIndex); scroll({smooth: true, y: 0})}} className="pricing-action btn-send btn-calc btn-inn-calc">{useLang(orderLang)} {useLang(pack.name)}</a>
        )}
      </ScrollTo>

                        </div>
                 <div className="paldenleft">
                        <h3>{useLang(pack.name)}</h3>
                        <ul className='pricing-feature-list' dangerouslySetInnerHTML={createMarkup(useLang(pack.featureDescriptions))}>
                        </ul></div>
                    </div>)}

        </div>

        <div className='pricing pricing-palden index1' id="nondeveloperka" /*style={isPopUp ? { position: 'absolute' } : {}}*/>
            {takeLast(3, selectedService.packs).map((pack, packIndex) => <div className={'pricing-item ' + ((packIndex === 2) ? 'pricing__item--featured' : '')}>
                        <div className={'pricing-deco' + ((packIndex === 3) ? 'elite' : '')}>
                            <div className='pricing-price'>{UseDiscount(discount, pack.price)}<span className='pricing-currency'> PLN</span><span className='pricing-period'>{useLang(startsFromLang)}</span>
                            </div>
                             <ScrollTo>
        {({ scroll }) => (
          <a onClick={() => { onSelect(packIndex); scroll({smooth: true, y: 0})}} className="pricing-action btn-send btn-calc btn-inn-calc">{useLang(orderLang)} {useLang(pack.name)}</a>
        )}
      </ScrollTo>
                        </div>
                   <div className="paldenleft">
                        <h3>{useLang(pack.name)}</h3>
                        <ul className='pricing-feature-list' dangerouslySetInnerHTML={createMarkup(useLang(pack.featureDescriptions))}>
                        </ul>
                      </div>
                    </div>)}

        </div>

        </div>);
     } else {
        return (

            <div className='pricing pricing-palden' /*style={isPopUp ? { position: 'absolute' } : {}}*/>
            {take(3, selectedService.packs).map((pack, packIndex) => <div className={'pricing-item ' + ((packIndex === 2) ? 'pricing__item--featured' : '')}>
                        <div className={'pricing-deco' + ((packIndex === 3) ? 'elite' : '')}>
                            <div className='pricing-price'>{UseDiscount(discount, pack.price)}<span className='pricing-currency'> PLN</span><span className='pricing-period'>{useLang(startsFromLang)}</span>
                            </div>
                             <ScrollTo>
        {({ scroll }) => (
          <a onClick={() => { onSelect(packIndex); scroll({smooth: true, y: 0})}} className="pricing-action btn-send btn-calc btn-inn-calc">{useLang(orderLang)} {useLang(pack.name)}</a>
        )}
      </ScrollTo>
                        </div>
                      <div className="paldenleft">
                        <h3>{useLang(pack.name)}</h3>
                        <ul className='pricing-feature-list' dangerouslySetInnerHTML={createMarkup(useLang(pack.featureDescriptions))}>
                        </ul>
                    </div>
                    </div>)}

        </div>);
    }
};
