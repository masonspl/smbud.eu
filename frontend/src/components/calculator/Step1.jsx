import React, {useRef, props} from 'react';
import useLang from '../../hooks/useLang';
import PacksList from './PacksList';
import { ScrollTo } from "react-scroll-to";

export const startsFromLang = {
    pl: '/ m2',
};
export const orderLang = {
    pl: 'Wybierz',
};
export const removeLang = {
    ru: 'Удалить',
    pl: 'Usuń',
    en: 'Remove',
};
export const modulesLang = {
    ru: 'Модули',
    pl: 'Moduły',
    en: 'Modules',
};
export const packageLang = {
    ru: '',
    pl: '',
    en: '',
};

export default ({ config, onSubmit, ref}) => {
    const [index, setIndex] = React.useState(undefined);
    const selectedSevice = config.services[index];
    const packs = selectedSevice ? selectedSevice.packs : [];
    let developerka = true;
    const myRef = React.createRef();

 /*useEffect(() => {
    Prices.current.scrollIntoView({ behavior: 'smooth' });
  });
    const Prices = useRef(null);*/

    console.log(developerka);

    return <section className="step-first step-container">
            <h3 className="subheader">Wybierz rodzaj usług</h3>
            <div className="services-pricing-grid">
                {config.services.map((service, curIndex) => <div className={"services-pricing-item " + (index === curIndex ? 'activated' : '')}>
                            <div className="pricing-item-descr">
                                <div className="pricing-item-header"><span>{Number(curIndex + 1)}. </span>{useLang(service.name)}</div>
                                <div className="pricing-item-content">{useLang(service.description)}
                                </div>
                            </div>
                            <div className="pricing-item-controls">
                                <div className="pricing-item-price">
                                    <span>już od</span> <small></small>{ `${service.packs[0].price}` }<small></small> <span>{useLang(startsFromLang)} netto.</span>
                                </div>
                                <div  className="btn-send btn-calc">
                            <ScrollTo>
        {({ scroll }) => (
          <a onClick={() => {setIndex(curIndex); scroll({smooth: true, y: myRef.current.offsetTop})}} className="pricing-action btn-send btn-calc btn-inn-calc">Oblicz</a>
        )}
      </ScrollTo>
                                    </div>
                            </div>
                        </div>)}
            </div><section className='prices'  ref={myRef}  >
            {index !== undefined && index !== 1 &&
         <section> <h3 className="subheader">{useLang('Выберите пакет', 'Select a package', 'Wybierz pakiet')}</h3><PacksList serviceIndex={index} config={config} onSelect={(packIndex) => onSubmit({
            products: [{
                    packIndex,
                    serviceIndex: index,
                    extraModules: 0,
                }]
        })}/></section>}
    {index !== undefined && index === 1 &&
         <section> <PacksList serviceIndex={index} config={config} onSelect={(packIndex) => onSubmit({
            products: [{
                    packIndex,
                    serviceIndex: index,
                    extraModules: 0,
                }]
        })}/></section>}

    </section>

    </section>;

};
