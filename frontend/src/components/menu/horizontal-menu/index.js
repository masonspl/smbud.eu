import React from 'react'
import {HorizontalNav} from './horizontal-menu.stc'
import NavBar, {NavItem, NavLink, Submenu} from '../../shared/navbar'
 
const HorizontalMenu = ({menuData, ...restProps}) => { 
    return (
        <HorizontalNav {...restProps}>
            <NavBar>
                {menuData.map(menu => {
                    const submenu = menu.node.submenu;
                    return (
                        <NavItem key={`mainmenu-${menu.node.id}`}>
                            <NavLink className="linker" path={menu.node.path || "/"}>{menu.node.title}</NavLink>
                        </NavItem>
                    )
                })}     
            </NavBar> 
        </HorizontalNav>
    )
} 

export default HorizontalMenu;