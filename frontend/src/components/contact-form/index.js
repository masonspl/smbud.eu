import React, { useState } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import { useForm } from "react-hook-form";
import Button from '../shared/button'
import Form, { Input, Textarea, FormGroup } from '../shared/form'
import { FormWrap } from './contact-form.stc'
import Cookies from 'js-cookie'

const ContactForm = ({ inputStyle, textareaStyle, url }) => {
    const { register, handleSubmit, getValues, errors, reset } = useForm({
        mode: "onBlur",
        reValidateMode: 'onChange',
    })

    const [serverState, setServerState] = useState({
        submitting: false,
        status: null
    });
    const [value, setValue] = useState({
        subject: ''
    });
      const onChangeHandler = e => {
        setValue({ ...value, [e.target.name]: e.target.value })
        console.log('CHANGES');
    }
    const handleServerResponse = (ok, msg, form) => {
        setServerState({
            submitting: false,
            status: { ok, msg }
        });
        if (ok) {
            form.reset();
            setValue({
                name: '',
                email: '',
                subject: '',
                message: ''
            })
        }
    };
    const onSubmit = (data, e) => {
        const form = e.target;
       setServerState({ submitting: true });
        fetch(`/api/pl/home/addOrder/`,{
          method: 'POST',
          headers: {
             'User-Agent': 'MyAgent (Insomnia)',
            'Content-Type': 'application/json'
            },
          body: JSON.stringify({ 'name': value.name, 'email': value.email, 'message':value.message, 'phone': value.phone, 'date': new Date().toISOString()})
        })
            .then(res => {
                handleServerResponse(true, "Dziękujemy! Twoja wiadomość została pomyślnie wysłana", form);
            })
            .catch(err => {
                handleServerResponse(false, err.response.data.error, form);
            });
    }

    return (
        <FormWrap>
            <Form onSubmit={handleSubmit(onSubmit) }>
                <div className="form-row">
                    <FormGroup className="halfwidth">
                        <Input
                            className="formcontr"
                            type="text"
                            name="name"
                            id="name"
                            placeholder="Imię"
                            onChange={onChangeHandler}
                            ref={register({
                                required: 'Imię wymagane',
                            })}
                            {...inputStyle}
                        />
                        {errors.name && <span className="error">{errors.name.message}</span>}
                    </FormGroup>
                    <FormGroup className="halfwidth">
                        <Input
                            type="email"
                            name="email"
                            className="formcontr"
                            id="email"
                            placeholder="Email"
                            onChange={onChangeHandler}
                            ref={register({
                                required: 'Email wymagany'
                            })}
                        />
                        {errors.email && <span className="error">{errors.email.message}</span>}
                    </FormGroup>
                    <FormGroup className="halfwidth halfbottom">
                        <Input
                            type="tel"
                            name="phone"
                            className="formcontr"
                            id="phone"
                            placeholder="Telefon"
                            onChange={onChangeHandler}
                            ref={register({
                                required: 'Telefon wymagany'
                            })}
                        />
                        {errors.email && <span className="error">{errors.email.message}</span>}
                    </FormGroup>
                </div>
                <div className="form-row">
                    <FormGroup {...textareaStyle}>
                        <Textarea
                            name="message"
                            className="formcontr"
                            placeholder="Wiadomość"
                            onChange={onChangeHandler}
                            ref={register({
                                required: 'Wpisz wiadomość',
                                minLength: { value: 10, message: "Min 10 symboli" }
                            })}
                        />
                        {errors.message && <span className="error">{errors.message.message}</span>}
                    </FormGroup>
                </div>
                <div className="form-row">
                    <FormGroup>
                        <div className="rodobox">
                          <input type="checkbox" id="scales" name="scales" required ref={register}/>
                          <label for="scales">&nbsp; Akceptuję <a href="https://smbud.eu/rodo" className="rodoboxa">obowiązki Informacyjne</a></label>
                        </div>
                    </FormGroup>
                </div><div className="form-row">
                    <FormGroup>
                        <Button className="sendbtn" type="submit" disabled={serverState.submitting}>Wyślij</Button>
                        {serverState.status && (
                            <p className={`form-output ${!serverState.status.ok ? "errorMsg" : "success"}`}>
                                {serverState.status.msg}
                            </p>
                        )}
                    </FormGroup>
                </div>
            </Form>
        </FormWrap>
    )
}


ContactForm.propTypes = {
    inputStyle: PropTypes.object,
    textareaStyle: PropTypes.object
}

ContactForm.defaultProps = {
    inputStyle: {
        responsive: {
            xsmall: {
                mb: '20px'
            }
        }
    },
    textareaStyle: {
        mt: '40px',
        mb: '40px',
        responsive: {
            xsmall: {
                mt: '25px',
                mb: '25px'
            }
        }
    }
}

export default ContactForm
