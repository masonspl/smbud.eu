import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'gatsby'
import Img from 'gatsby-image'
import Image from '../image'
import {ProjectWrapper, Thumb, ProjectBtnWrap, ProjectBtn, ProjectType} from './project.stc'
import Heading from '../shared/heading'
import Text from '../shared/text'
import { Controller, Scene } from 'react-scrollmagic';
  
const Project = ({image, id, slug, address, category, title, client, sector, isEven, ...restProps}) => {
    const {HeadingStyle, TextStyle, ...restStyles} = restProps
    console.log(this);
    return (
            <div className={`col-1 portfolioitem`}>
                <Thumb>
                    <div id={id}></div>
                    <Controller>
                        <Link to={`/project/${slug}`}>
                        <Scene classToggle="animated" triggerElement={`#${id}`} triggerHook="onCenter">
                            <div className="rn_surface">
                                   <img src={'https://smbud.eu'+image}  alt={title}/>
                            </div>
                        </Scene>
                            <div className="wow fadeInLeft" style={{position: 'absolute', color: '#fff', bottom: '25px', left: '15px'}}data-wow-delay="200ms" data-wow-duration="1000ms">
                                <h3 style={{lineHeight: '1em'}}>{title}</h3>
                                <span>{address}</span>
                            </div>
                        </Link>
                    </Controller>
                </Thumb>
            </div>
    )
}

Project.propTypes = {
    HeadingStyle: PropTypes.object,
    TextStyle: PropTypes.object
}

Project.defaultProps = {
    HeadingStyle: {
        as: 'h6',
        color: '#001c43',
        fontSize: '12px',
        fontweight: 700,
        letterspacing: '2px',
        mb: '12px',
        responsive: {
            small: {
                letterspacing: '1px',
                mb: '5px'
            }
        }
    },
    TextStyle: {
        fontSize: '12px',
        letterspacing: '1px',
        mb: 0
    }
}

export default Project;