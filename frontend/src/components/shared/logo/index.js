import React from 'react'
import {Link} from 'gatsby'
import {LogoWrapper} from './logo.stc';
import logo from '../../../assets/img/logo/sm-logo-white.svg';
import logoBlack from '../../../assets/img/logo/sm-logo.svg';

const Logo = (props) => {
    return (
        <LogoWrapper {...props}>
            <Link to="/" className="big-cursor">
                <img src={logo} alt="creative agency" className="white-logo" width={50}/>
                <img src={logoBlack} alt="creative agency" className="black-logo" width={50}/>
            </Link>
        </LogoWrapper>
    )
}

export default Logo
