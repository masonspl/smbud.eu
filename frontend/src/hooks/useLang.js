
export const getLang = () => {
    let id = 'pl';
    return id;
};
export default (langOrRu, en = langOrRu, pl = en) => {
    const lang = typeof langOrRu === 'string' ? { ru: langOrRu, en, pl } : langOrRu;
    let id = getLang();
    return lang[id];
};
