import React, {Component} from "react"
import SEO from '../components/seo';
import Layout from '../containers/layout/layout'
import { render } from 'react-dom'
import Pricing from '../components/calculator/PricingPage'
import { Field } from 'react-final-form'

class Calculator extends Component{

constructor(props){
        super(props);
        this.state = {

        };
    }

  render(){

        return(
    <Layout headerLayout={2} headerStyle="darkHeader">
        <SEO title="Calkulator"/>
        <div className="row">
        <div className="container">
        <div className="policy mt-30">
            <Pricing />
        </div>
        </div>
        </div>
    </Layout>
        )
    }
}
export default Calculator;