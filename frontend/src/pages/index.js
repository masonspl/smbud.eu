import React from "react"
import SEO from '../components/seo';
import Layout from '../containers/layout/layout'
import Hero from '../containers/home-slider/hero'
import Story from '../containers/home-slider/story'
import axios from "axios";
import ProjectSection from '../containers/home-slider/project'
import TestimonialSection from '../containers/home-slider/testimonial'
import ClientSection from '../containers/home-slider/clients'
axios.post('/__refresh')

export default () => (
    <Layout headerLayout={2}>
        <SEO title="Strona Główna"/>
        <Hero/>
        <Story/>
        <ProjectSection/>
        <TestimonialSection/>
        <ClientSection/>
    </Layout>
)
 