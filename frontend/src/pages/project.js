import React from "react"
import { Router } from "@reach/router"
import Banner from '../containers/projects/banner'
import ProjectsList from '../containers/projects/project'
import ProjectTemplate from '../templates/project-template/project-template'

export default () => (
         <Router>
         <ProjectsList path="/project" />
         <ProjectTemplate path="/project/:slug" />
         </Router>
)
