import React from "react"
import SEO from '../components/seo';
import Layout from '../containers/layout/layout'
import Banner from '../containers/company/banner'

export default () => (
    <Layout headerLayout={2}>
        <SEO title="Company"/>
        <Banner/>
        <div className="row">
        <div className="container">
        <div className="policy">
                <h1>Polityka Prywatności</h1>
                <p>Poniższa Polityka Cookies określa zasady zapisywania i uzyskiwania dostępu do danych na Urządzeniach Użytkowników korzystających z Serwisu do celów świadczenia usług drogą elektroniczną przez Administratora Serwisu.</p>
<h3>§ 1 Definicje</h3>
<p>Serwis - serwis internetowy działający pod adresem https://smbud.eu/</p>
<p>Serwis zewnętrzny - serwis internetowe partnerów, usługodawców lub usługobiorców Administratora</p>
<p>Administrator>Administrator - firma SmBud Sp. z o.o., prowadząca działalność pod adresem: ul. Al. "Solidarności" 155/13, 00-877 Warszawa, Polska, o nadanym numerze identyfikacji podatkowej (NIP): 5213652924, świadcząca usługi drogą elektroniczną za pośrednictwem Serwisu oraz przechowująca i uzyskująca dostęp do informacji w urządzeniach Użytkownika</p>
<p>Użytkownik - osba fizyczna, dla której Administrator świadczy usługi drogą elektroniczna za pośrednictwem Serwisu.</p>
<p>Urządzenie - elektroniczne urządzenie wraz z oprogramowaniem, za pośrednictwem, którego Użytkownik uzyskuje dostęp do Serwisu</p>
<p>Cookies (ciasteczka) - dane tekstowe gromadzone w formie plików zamieszczanych na Urządzeniu Użytkownika</p>
<h3>§ 2 Rodzaje Cookies</h3>
<p>Cookies wewnętrzne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przes system teleinformatyczny Serwisu</p>
<p>Cookies zewnętrzne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez systemy teleinformatyczne Serwisów zewnętrznych</p>
<p>Cookies sesyjne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez Serwis lub Serwisy zewnętrzne podczas jednej sesji danego Urządzenia. Po zakończeniu sesji pliki są usuwane z Urządzenia Użytkownika.</p>
<p>Cookies trwałe - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez Serwis lub Serwisy zewnętrzne do momentu ich ręcznego usunięcia. Pliki nie są usuwane automatycznie po zakończeniu sesji Urządzenia chyba że konfiguracja Urządzenia Użytkownika jest ustawiona na tryb usuwanie plików Cookie po zakończeniu sesji Urządzenia.</p>
<h3>§ 3 Bezpieczeństwo</h3>
<p>Mechanizmy składowania i odczytu - Mechanizmy składowania i odczytu Cookies nie pozwalają na pobierania jakichkolwiek danych osobowych ani żadnych informacji poufnych z Urządzenia Użytkownika. Przeniesienie na Urządzenie Użytkownika wirusów, koni trojańskich oraz innych robaków jest praktynie niemożliwe.</p>
<p>Cookie wewnętrzne - zastosowane przez Administratora Cookie wewnętrzne są bezpieczne dla Urządzeń Użytkowników</p>
<p>Cookie zewnętrzne - za bezpieczeństwo plików Cookie pochodzących od partnerów Serwisu Administrator nie ponosi odpowiedzialności. Lista partnerów zamieszczona jest w dalszej części Polityki Cookie.</p>
<h3>§ 4 Cele do których wykorzystywane są pliki Cookie</h3>
<p>Usprawnienie i ułatwienie dostępu do Serwisu - Administrator może przechowywać w plikach Cookie informacje o prefernecjach i ustawieniach użytkownika dotyczących Serwisu aby usprawnić, polepszyć i przyśpieszyć świadczenie usług w ramach Serwisu.</p>
<p>Logowanie>Logowanie - Administrator wykorzystuje pliki Cookie do celów logowania Użytkowników w Serwisie</p>
<p>Marketing i reklama - Administrator oraz Serwisy zewnętrzne wykorzystują pliki Cookie do celów marketingowych oraz serwowania reklam Użytkowników.</p>
<p>Dane statystyczne - Administrator oraz Serwisy zewnętrzne wykorzystuje pliki Cookie do zbirania i przetwarzania danych statystycznych takich jak np. statystyki odwiedzin, statystyki Urządzeń Użytkowników czy statystyki zachowań użytkowników. Dane te zbierane są w celu analizy i ulepszania Serwisu.</p>
<p>Serwowanie usług multimedialnych - Administrator oraz Serwisy zewnętrzne wykorzystuje pliki Cookie do serwowania Użytkownikom usług multimedialnych.</p>
<p>Usługi społecznościowe - Administrator oraz Serwisy zewnętrzne wykorzystują pliki Cookie do wsparcia usług społecznościowych</p>
<h3>§ 5 Serwisy zewnętrzne</h3>
<p>Administrator współpracuje z następującymi serwisami zewnętrznymi, które mogą zamieszczać pliki Cookie na Urządzeniach Użytkownika:</p>
<p>Google Analytics</p>
<h3>§ 6 Możliwości określania warunków przechowywania i uzyskiwania dostępu na Urządzeniach Użytkownika przez Serwis i Serwisy zewnętrzne</h3>
<p>Użytkownik może w dowolnym momencie, samodzielnie zmienić ustawienia dotyczące zapisywania, usuwania oraz dostępu do danych zapisanych plików Cookies</p>
<p>Informacje o sposobie wyłączenia plików Cookie w najpopularniejszych przeglądarkach komputerowych i urządzeń mobilnych dostępna są na stronie: jak wyłączyć cookie.</p>
<p>Użytkownik może w dowolnym momencie usunąć wszelkie zapisane do tej pory pliki Cookie korzystając z narzędzi Urządzenia Użytkownika za pośrednictwem którego Użytkowanik korzysta z usług Serwisu.</p>
<h3>§ 7 Wymagania Serwisu</h3>
<p>Ograniczenie zapisu i dostępu do plików Cookie na Urządzeniu Użytkownika może spowodować nieprawidłowe działanie niektórych funkcji Serwisu.</p>
<p>Administrator nie ponosi żadnej odpowiedzialności za nieprawidłowo działające funkcje Serwisu w przypadku gdy Użytkownik ograniczy w jakikolwiek sposób możliwość zapisywania i odczytu plików Cookie.</p>
<h3>§ 8 Zmiany w Polityce Cookie</h3>
<p>Administrator zastrzega sobie prawo do dowolnej zmiany niniejszej Polityki Cookie bez konieczności zawiadomienia użytkowników</p>

        </div>
        </div>
        </div>
    </Layout>
)
