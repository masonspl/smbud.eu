import React from "react"
import SEO from '../components/seo';
import Layout from '../containers/layout/layout'
import Banner from '../containers/company/banner'

export default () => (
    <Layout headerLayout={2}>
        <SEO title="Company"/>
        <Banner/>
        <div className="row">
        <div className="container">
        <div className="policy">
              <h3>Obowiązki Informacyjne</h3>
<p>W związku z realizacją wymogów Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepły-wu takich danych oraz uchylenia dyrektywy 95/46/WE („RODO”) informujemy o zasadach przetwarzania danych osobowych osób fizycznych i przysługujących tym osobom fizycznym prawach związanych z przetwarzaniem.</p>
 <p>Obowiązkiem ochrony będą objęte osoby fizyczne dokonujące czynności prawnej niezwiązanej bezpośrednio z ich dzia-łalnością (jeżeli uzyskane zostaną ich dane osobowe poza paragon), osoby fizyczne prowadzące działalność gospodarczą lub zawodową oraz osoby fizyczne reprezentujące lub upoważnione do reprezentacji czy kontaktu osób prawnych albo jednostek organizacyjnych.</p>
<h4>1.	Administrator danych osobowych:</h4>
<p>Administratorem danych osobowych jest SM BUD Spółka z ograniczona odpowiedzialnoscia, al. Solidarności 155/13, 00-877 Warszawa, , zarejestrowaną w rejestrze przedsiębiorców prowadzonym przez Sąd Rejonowy dla m.st. Warszawy w War-szawie, XII Wydział Gospodarczy Krajowego Rejestru Sadowego pod numerem KRS 0000735573, NIP: 527-28-55-333, REGON: 380469604, kapitał zakładowy: 5.000,00 zł.</p>
<h4> 2.         Rodzaje przetwarzanych danych osobowych:</h4>
<p>Administrator w związku z zawieraniem umów handlowych będzie przetwarzał dane osobowe uzyskane podczas zawie-rania umów oraz w trakcie trwania i całkowitego rozliczenia umów takie jak, w szczególności:</p>
<p>•	imię (imiona) i nazwisko, adres e-mail, adres do kontaktu, firma reprezentowanego podmiotu, numer telefonu, numer faksu, stanowisko</p>
<p>•	w przypadku przedsiębiorców przetwarzamy dodatkowo: NIP, REGON, adres siedziby,</p>
<p>•	w przypadku przedsiębiorców wnioskujących o udzielenie limitu kredytowego przetwarzamy również dane zawarte w dokumentach finansowych.</p>
<h4>3.         Cele przetwarzania danych osobowych:</h4>
<p>Gromadzimy dane osobowe w następujących celach:</p>
<p>•	zawierania, w tym przesyłania konkretnych ofert i realizacji łączącej nas umowy oraz świadczenia dla Państwa ofero-wanych przez nas dostaw i usług, realizacji płatności, obsługi reklamacji itp. (art. 6 ust. 1 lit. b RODO),</p>
<p>•	realizacji obowiązków przewidzianych prawem, w szczególności wystawiania i przechowywania faktur i innych doku-mentów księgowych, spełniania celów archiwizacyjnych itp. (art. 6 ust. 1 lit. c RODO),</p>
<p>•	ochrony praw zgodnie z przepisami prawa, w szczególności ustalenia, dochodzenia lub obrony przed roszczeniami związanymi z zawartymi umowami lub przetwarzaniem danych osobowych jako niezbędne do realizacji prawnie uzasadnionego interesu Administratora (art. 6 ust. 1 lit. f RODO),</p>
<p>•	badania satysfakcji z dostaw towarów i świadczenia usług jako niezbędne do realizacji prawnie uzasadnionego interesu Administratora przez podwyższanie standardów realizacyjnych (art. 6 ust. 1 lit. f RODO),</p>
<p>•	gromadzenia, analizowania i przekazywania danych sprzedażowych jako niezbędne do realizacji prawnie uzasadnio-nego interesu przez możliwość zagwarantowania uczestnictwa w akcjach promocyjnych i bonusowych Administratora, produ-centów towarów czy grupy zakupowej ABG (art. 6 ust. 1 lit. f RODO),</p>
<p>•	weryfikacji wiarygodności płatniczej i zdolności kredytowej przy udzielaniu limitu kredytowego jak również w celu za-bezpieczenia należnych płatności (umowy poręczenia oraz inne zabezpieczenia) jako niezbędne do realizacji prawnie uzasadnio-nego interesu Administratora ograniczenia konieczności ustalenia, dochodzenia lub obrony przed roszczeniami (art. 6 ust. 1 lit. f RODO),</p>
<p>•	przeprowadzenia bezpośredniego marketingu produktów jako niezbędne do realizacji prawnie uzasadnionego interesu Administratora poszerzania oferty handlowej (art. 6 ust. 1 lit. f RODO),</p>
<p>•	gromadzenia osobnych zgód na przesyłanie informacji handlowych i przeprowadzanie działań marketingowych za po-średnictwem środków elektronicznego przekazu i usług telefonicznych, w tym za pośrednictwem SMS oraz MMS, które są wy-łączone z zakresu bezpośredniego marketingu produktów (art. 6 ust. 1 lit. a RODO),</p>
<p>•	gromadzenia osobnych zgód na uczestnictwo w akcjach promocyjnych organizowanych przez Administratora, produ-centów towarów czy grupy zakupowej ABG (art. 6 ust. 1 lit. f RODO),</p>
<p>•	w pozostałych przypadkach przetwarzania danych osobowych będą przetwarzane wyłącznie na podstawie wcześniej udzielonej zgody w zakresie i celu określonym w treści zgody (art. 6 ust. 1 lit. a RODO),</p>
<h4>4.         Kategorie odbiorców danych osobowych:</h4>
<p>Odbiorcami danych osobowych mogą być podmioty z następujących kategorii odbiorców:</p>
<p>•	upoważnieni pracownicy lub współpracownicy Administratora,</p>
<p>•	producenci towarów,</p>
<p>•	nabywcy wierzytelności,</p>
<p>•	dalsi sprzedawcy usług lub towarów,</p>
<p>•	świadczące usługi, w szczególności informatyczne i telekomunikacyjne, obsługi klienta, księgowo – finansowe, dorad-cze, audytorskie i kontrolne, windykacyjne i prawne, drukarskie i archiwizacyjne, pocztowe i kurierskie, bankowe i ubezpiecze-niowe, magazynowe i przewozowe, marketingowe i promocyjne itp.</p>
<p>•	organom nadzorującym, organom władzy i innym osobom trzecim; w przypadku, gdy jest to niezbędne dla realizacji celów wskazanych powyżej oraz wypełnienia obowiązków nałożonych prawem,</p>
<p>•	inne podmioty, z którymi Administrator zawarł umowy powierzenia przetwarzania danych osobowych. Dane osobowe będą przetwarzać wyłącznie zgodnie z instrukcjami przekazanymi przez Administratora.</p>
<h4>5.         Przekazywanie danych osobowych:</h4>
<p>Dane osobowe nie będą przekazywane poza Europejski Obszar Gospodarczy obejmujący Unię Europejską (UE), Norwe-gię, Liechtenstein i Islandię.</p>
<h4>6.         Okres przechowywania danych osobowych:</h4>
<p>Dane osobowe będą przechowywane do czasu cofnięcia odrębnej zgody albo do momentu przedawnienia roszczeń zwią-zanych z dostawą towarów lub świadczeniem usług albo do momentu wygaśnięcia obowiązków prawnych, w szczególności wy-nikających z przepisów rachunkowych, podatkowych i archiwizacyjnych tj. wyłącznie przez czas niezbędny do osiągnięcia celów, dla których dane te są gromadzone.</p>
<h4>7.         Sposób przechowywania danych osobowych:</h4>
<p>Administrator wdrożył odpowiednie środki bezpieczeństwa, zarówno techniczne jak i organizacyjne, aby chronić dane osobowe, w tym szyfrowanie danych, ciągłe zapewnienie poufności, integralności, dostępności i odporności systemów oraz zdolność do szybkiego przywrócenia danych osobowych. Ponadto, Administrator zobowiązuje się do wyboru podmiotów prze-twarzających gwarantujących odpowiednią ochronę danych osobowych.</p>
<h4>8.         Przysługujące prawa do danych osobowych:</h4>
<p>·           prawo dostępu do treści danych (art. 15 RODO);</p>
<p>·           prawo do sprostowania i poprawienia danych (art. 16 RODO);</p>
<p>·           prawo do usunięcia danych (art. 17 RODO);</p>
<p>·           prawo do ograniczenia przetwarzania danych (art. 18 RODO);</p>
<p>·           prawo do przenoszenia danych (art. 20 RODO);</p>
<p>·           prawo do wniesienia sprzeciwu (art. 21 RODO);</p>
<p>·           prawo do wniesienia skargi do organu nadzorczego Prezesa Urzędu Ochrony Danych Osobowych (art. 80 RODO);</p>
<h4>9.         Automatyzacja i profilowanie:</h4>
<p>Dane osobowe mogą być przetwarzane w sposób zautomatyzowany, ale nie będą profilowane.</p>
<h4>10.      Warunki udzielenia odrębnej zgody:</h4>
<p>Zgoda musi zostać wyrażona świadomie i dobrowolnie na konkretny zakres przetwarzania danych osobowych, cofnięcie zgody jest możliwe w dowolnym momencie bez spełniania jakichkolwiek warunków i w dowolny sposób tylko tak, aby doszła do wiadomości Administratora. Jednakże, cofnięcie zgody pozostanie bez wpływu na zgodność z prawem przetwarzania danych, którego dokonano na podstawie zgody przed jej cofnięciem oraz pozostanie bez wpływu w zakresie, gdy na Administratorze ciąży dalszy obowiązek prawny ich przetwarzania lub wynika to z prawnie uzasadnionych interesów administratora, w tym do czasu przedawnienia ewentualnych roszczeń.</p>
<h4>11.     Informacja o konsekwencjach niepodania danych osobowych:</h4>
<p>Podanie danych osobowych jest dobrowolne, jednak odmowa ich podania uniemożliwia zawarcie i realizację umów han-dlowych albo innych czynności faktycznych.</p>
<h4>12.     Dane Kontaktowe:</h4>
<p>W przypadku jakichkolwiek pytań dotyczących danych osobowych lub wykonywania praw w zakresie prywatności, pro-simy o kontakt Administratorem pod adresem: 00-877 Warszawa, ul. Al. „Solidarności” 155/13, tel. + 48 570901091 lub pod adresem mailowym: biuro@smbudspzoo.pl</p>
<p>Inspektor danych osobowych nie zostaje powołany, gdyż dane nie są przetwarzane przez podmioty z sektora publiczne-go, główna działalność Administratora nie polega na operacjach przetwarzania, które ze względu na swój charakter, zakres lub cele wymagają regularnego i systematycznego monitorowania osób, których dane dotyczą, na dużą skalę oraz główna działal-ność Administratora nie polega na przetwarzaniu na dużą skalę szczególnych kategorii danych osobowych lub danych osobo-wych dotyczących wyroków skazujących.</p>
<p>Informujemy o możliwości okresowej aktualizacji Obowiązków Informacyjnych zgodnie z obowiązującym prawem i w ta-kim przypadku będziemy odpowiednio informować o wprowadzanych zmianach.</p>

        </div>
        </div>
        </div>
    </Layout>
)
