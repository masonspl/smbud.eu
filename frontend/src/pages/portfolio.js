import React from "react"
import SEO from '../components/seo';
import Layout from '../containers/layout/layout'
import Portfolio from "../containers/company/portfolio";


export default () => (
    <Layout headerLayout={2} headerStyle="darkHeader">
        <SEO title="Portfolio"/>
        <Portfolio/>
    </Layout>
)
