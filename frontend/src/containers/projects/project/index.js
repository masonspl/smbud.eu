import React, {Component} from 'react'
import SectionTitle from '../../../components/shared/section-title'
import Project from '../../../components/project'
import Layout from '../../../containers/layout/layout'

class ProjectSection extends Component{
    constructor (props) {
        super (props)
        this.state = {
            items: [],
        }
    }
    componentDidMount(){
        fetch('https://smbud.eu/api/pl/portfolio/getPortfolioItems/?format=json')
        .then(res => res.json())
        .then(json => {
                this.setState({items: json});
                console.log(this);
        });
    }
    render() {
    return (
        <Layout headerLayout={2} headerStyle="darkHeader">
            <div className="project-container">
                <div className="col-1 offset-1">
                    <SectionTitle
                        title="Nasze portfolio"
                        subtitle="Sprwdź nasze projekty"
                    />
                </div>

                <div className="project-wrapper row col-4 offset-1">
                    {this.state.items.map((data, i) => {
                        let isEven = i%2 === 0;
                        return(
                            <Project
                                isEven={isEven}
                                key={data.id}
                                id={data.id}
                                slug={data.CURL}
                                image={data.thumbnail}
                                address={data.address}
                                title={data.name}
                                sector={data.category.name}
                            />
                        )
                    })}
                </div>
            </div>
        </Layout>
    )
}
}


export default ProjectSection
