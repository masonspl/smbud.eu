import React from 'react';
import { useStaticQuery, graphql } from "gatsby"
import Image from '../../../components/image'
import {BannerWrap} from './banner.stc'


const Banner = (image) => {
    const banngerImg = image.image;
    console.log(banngerImg);
    return(
        <BannerWrap>
            <img src={'https://smbud.eu/'+banngerImg} alt="projectBanner"/>
        </BannerWrap>
    )
}

export const query = graphql `
    query($slug: String){
        dsidePort(slug: {eq: $slug}){
            id
            name
            main_img
        }
    }
`;
export default Banner;