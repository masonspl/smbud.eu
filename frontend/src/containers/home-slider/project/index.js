import React from 'react'
import PropTypes from 'prop-types'
import { useStaticQuery, graphql } from "gatsby"
import SectionTitle from '../../../components/shared/section-title'
import Project from '../../../components/project'
import {ProjectSectionWrap} from './project.stc'
 
const ProjectSection = ({section, project}) => {
    const projectHomeQueryData = useStaticQuery(graphql `
    query ProjectHomeDataQuery {
      homedataJson(id: {eq: "project_section_content"}) {
        id
        title
        subtitle
      }
      allDsidePort {
        edges {
          node {
            id
            name
            thumb
            slug
            address
            category {
             name
            }
          }
        }
      }
    }  
    `);
    const projectSecData = projectHomeQueryData.homedataJson;
    const projectsData = projectHomeQueryData.allDsidePort.edges;
    return (
        <ProjectSectionWrap {...section}>
            <div className="project-container">
                <div className="col-1 offset-1">
                    <SectionTitle
                        title={projectSecData.title}
                        subtitle={projectSecData.subtitle}
                    />
                </div>

                <div className="project-wrapper col-4 offset-1">
                    {projectsData.map((data, i) => {
                        let isEven = i%2 === 0;
                        return(
                            <Project
                                {...project}
                                isEven={isEven}
                                key={data.node.id}
                                id={data.node.id}
                                slug={data.node.slug}
                                image={data.node.thumb}
                                address={data.node.address}
                                title={data.node.name}
                                sector={data.node.category.name}
                            />
                        )
                    })}
                </div>
            </div>
        </ProjectSectionWrap>
    )
}

ProjectSection.propTypes = {
    section: PropTypes.object,
    project: PropTypes.object
}

ProjectSection.defaultProps = {
    section: {
        backgroundColor: '#f8f8f8'
    },
    project: {
        mt: '100px',
        responsive: {
            medium: {
                mt: '50px'
            }
        }
    }
}

export default ProjectSection
