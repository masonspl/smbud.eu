import React, {Fragment} from 'react';
import { useStaticQuery, graphql } from "gatsby"
import BlogHeading from '../blog-heading'
import Blog from '../../../components/blog/layout-one'
import Pagination from '../../../components/pagination'
import {SectionWrap, BlogInner} from './blog-area.stc'
 
const BlogArea = () => {
 const blogQueryData = useStaticQuery(graphql`
        query BlogDataQuery {
  allDsideBlog {
    edges {
      node {
        id
        main_img
        postid
        slug
        thumb
        title
        description
        date
      }
    }
  }
}

    `);
    
    const blogs = blogQueryData.allDsideBlog.edges;
    const {totalCount} = blogQueryData.allDsideBlog;
    const postsPerPage = 4;
    const numberOfPages = Math.ceil(totalCount/postsPerPage);
    return (
      <Fragment>
        <SectionWrap>
            <BlogHeading/>
            <BlogInner>
                <div className="col-4 offset-1">
                    <div className="row">
                        {blogs.map((blog, i) => (
                            <div className="col-2 blog-wrap" key={`blog-${i}`}>
                                <Blog
                                    title={blog.node.title}
                                    date={blog.node.date.substr(0, 10)}
                                    author='SM BUD sp. z o.o.'
                                    id={blog.node.postid}
                                    path={blog.node.slug}
                                    excerpt={blog.node.description}
                                    image={blog.node.thumb}
                                />
                            </div>
                        ))}
                    </div>
                </div>
            </BlogInner>
        </SectionWrap>
        <Pagination
            currentPage={1}
            numberOfPages={numberOfPages}
        />
      </Fragment>
    )
}

export default BlogArea