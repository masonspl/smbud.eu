import React from 'react';
import {Link} from 'gatsby';
import {FooterMenuWrap, FooterMenuList} from './footer-menu.stc'

const FooterMenu = () => {
    return (
        <FooterMenuWrap>

            <FooterMenuList>
                <Link to="/policy" className="no-cursor">Polityka Cookies</Link>
                <Link to="/rodo" className="no-cursor">RODO</Link>
            </FooterMenuList>
        </FooterMenuWrap>
    )
}

export default FooterMenu;