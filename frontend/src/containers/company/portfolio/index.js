import React, {Component} from 'react';
import { Link } from "gatsby";
import Heading from '../../../components/shared/heading'
import Text from '../../../components/shared/text'
import {PortfolioSectionWrap, SectionTitle, AboutContent} from './about.stc'



class Portfolio extends React.Component {
  state = {
    isLoading: true,
    categories: [],
    error: null
  };
fetchCategories() {
    fetch(`https://cors-anywhere.herokuapp.com/https://smbud.eu/api/pl/portfolio/getAllCategories/`)
      .then(response => response.json())
      .then(data =>
        this.setState({
          categories: data,
          isLoading: false,
        })
      )
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchCategories();
    console.log(this.state.categories);
  }
  render() {
const { isLoading, categories, error } = this.state;
    return (
        <React.Fragment>
        <PortfolioSectionWrap>
            <div className="row">
                    <div className="col-1 offset-1">
                        <h1>Portfolio</h1>
                    </div>
            </div>
            <div className="row">
                  <div className="col-1 offset-1">
                       Nasi wspaniałe projekty
                    </div>
            </div>
            <div className="row mt-50">
            <div className="col-4 offset-1">
                <div className="row">
        {error ? <p>{error.message}</p> : null}
        {!isLoading ? (
          categories.map(categories => {
            const { thumbnail, tag, name } = categories;
            return (
                <div className="col-2">
                    <Link to={'/portfolio/'+tag}>
              <div key={tag}>
                <img src={'https://smbud.eu/'+thumbnail}/>
                <h3>{name}</h3>
                <hr />
              </div>
                        </Link>
                </div>
            );
          })
        ) : (
          <h3>Loading...</h3>
        )}
         </div>
            </div>
            </div>
        </PortfolioSectionWrap>
      </React.Fragment>
    );
  }
}

export default Portfolio;