from rest_framework.views import APIView
from rest_framework.response import Response

from smbud.functions import increment_view
from portfolio.models import *
import django.utils.timezone


class CategoryList(APIView):

    def get(self, request, format="json", lang_code=None):

        response = []
        for x in Category.objects.all():
            try:
                response.append({
                    "name": x.categorytranslation_set.get(lang_code=lang_code).name,
                    "tag": x.tag,
                    "thumbnail": x.thumbnail.url,
                    "color": x.color
                })
            except CategoryTranslation.DoesNotExist:
                continue
        return Response(response)


class PortfolioItemList(APIView):

    def get(self, request, format=None, lang_code=None, category=None, home=None):

        response = []

        portfolio_items = PortfolioItem.objects.filter(
            category__tag=category) if category else PortfolioItem.objects.filter()

        portfolio_items = portfolio_items.filter(date__lt=django.utils.timezone.now())


        for x in portfolio_items:
            try:
                response.append({
                    "id": x.id,
                    "name": x.name,
                    "category": {
                        "name": x.category.categorytranslation_set.get().name,
                        "color": x.category.color,
                        "tag": x.category.tag
                    },
                    "thumbnail": x.thumbnail.url,
                    "main_img": x.main_image.url,
                    "second_img": x.second_image.url,
                    "address": x.address,
                    "CURL": x.base_name,
                })
            except (): #CategoryTranslation.DoesNotExist, PortfolioTranslation.DoesNotExist):
                continue
        return Response(response)


class PortfolioHomeItemList(PortfolioItemList):
    def get(self, request, format=None, lang_code=None, category=None, home=None):
        return super(PortfolioHomeItemList, self).get(request, format, lang_code, category, home=True)


class PortfolioDetails(APIView):
    def get(self, request, format=None, lang_code=None, cURL=None):

        response = []

        pi = PortfolioItem.objects.get(base_name=cURL)
        response = {
            "name": pi.name,
            "address": pi.address,
            "category":  pi.category.tag,
            "main_image": pi.main_image.url,
            "second_image": pi.second_image.url,
            "CURL": pi.base_name,
        }

        return Response(response)
