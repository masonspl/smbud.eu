from django.db import models
import django.utils.timezone
from smbud.lang_codes import LANGUAGES


class Category(models.Model):
    tag = models.CharField(max_length=50, unique=True)
    color = models.CharField(max_length=10, verbose_name="HEX Color")
    thumbnail = models.ImageField(blank=True)

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.tag


class CategoryTranslation(models.Model):
    name = models.CharField(max_length=100)
    lang_code = models.CharField(choices=LANGUAGES, max_length=5, verbose_name="Language")
    category = models.ForeignKey('Category', on_delete=models.CASCADE)


class Attachment(models.Model):
    kind = models.CharField(verbose_name="type", choices=(("video", "Video"), ("image", "Image")), max_length=10)
    content = models.FileField()
    portfolio_item = models.ForeignKey('PortfolioItem', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.kind, self.id)


class Attachment2(models.Model):
    kind = models.CharField(verbose_name="type", choices=(("html", "HTML"), ("image", "Image")), max_length=10)
    html = models.TextField(blank=True)
    image = models.ImageField(blank=True)
    portfolio_item = models.ForeignKey('PortfolioItem', on_delete=models.CASCADE)

    def __str__(self):
        return "{} {}".format(self.kind, self.id)


class PortfolioTranslation(models.Model):
    lang_code = models.CharField(choices=LANGUAGES, max_length=5, verbose_name="Language")
    name = models.CharField(max_length=100)
    description = models.TextField()

    portfolio_item = models.ForeignKey('PortfolioItem', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class PortfolioItem(models.Model):
    base_name = models.CharField(max_length=100, verbose_name="Technical name(only for panel)", unique=True)
    name = models.CharField(max_length=255, verbose_name="Name for users", unique=True)
    date = models.DateTimeField(default=django.utils.timezone.now)
    thumbnail = models.ImageField()
    main_image = models.ImageField()
    second_image = models.ImageField(blank=True)
    address = models.CharField(max_length=255, verbose_name="Project address", default=None, blank=True)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)

    class Meta:
        ordering = ("date",)

    def __str__(self):
        return self.base_name
