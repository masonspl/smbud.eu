from django import forms
from django.contrib import admin
# Register your models here.
from trumbowyg.widgets import TrumbowygWidget

from portfolio.models import *


class PortfolioItemForm(forms.ModelForm):
    class Meta:
        model = PortfolioTranslation
        widgets = {
            'description': TrumbowygWidget(),
            'task': TrumbowygWidget(),
            'decision': TrumbowygWidget(),
        }
        exclude = ('id',)



class CategoryTranslationInline(admin.StackedInline):
    verbose_name_plural = "category translations"
    verbose_name = "translation"
    model = CategoryTranslation

    extra = 1


class PortfolioTranslationInline(admin.StackedInline):
    verbose_name_plural = "portfolio translations"
    verbose_name = "translation"
    model = PortfolioTranslation
    form = PortfolioItemForm
    extra = 1



class CategoryAdmin(admin.ModelAdmin):
    list_display = ('tag',)
    inline = (CategoryTranslationInline)


class PortfolioItemAdmin(admin.ModelAdmin):
    list_display = ('base_name', 'category', 'date')
    list_filter = ('date', 'category')
    search_fields = ('description', 'name')
    inline = (PortfolioTranslationInline)


admin.site.register(PortfolioItem, PortfolioItemAdmin)
admin.site.register(Category, CategoryAdmin)
# admin.site.register()
# admin.site.register()
