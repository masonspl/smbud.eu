from rest_framework import serializers

from calculator.models import calculatorItem, TopicSuggestion


class calculatorItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = calculatorItem
        fields = '__all__'


class TopicSuggestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TopicSuggestion
        fields = '__all__'


