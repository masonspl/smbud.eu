from django.conf.urls import url

from calculator.views import CreateTopicSuggestion, calculatorCategoryList, calculatorItemDetails, calculatorItemList
from home.views import getUpdates

urlpatterns = [

    # url(r'^getReviewList/$', ReviewList.as_view()),

    # url(r'^getReviewDetails/(?P<id>[^.]+)/$', ReviewDetails.as_view()),

    url(r'^addTopicSuggestion/$', CreateTopicSuggestion.as_view()),
    url(r'^getAllCategories/$', calculatorCategoryList.as_view()),
    url(r'^getUpdates/$', getUpdates.as_view()),

    url(r'^getcalculatorItems/(?P<category>[^.]+)/$', calculatorItemList.as_view()),
    url(r'^getcalculatorItems/$', calculatorItemList.as_view()),
    url(r'^getcalculatorItemDetails/(?P<base_name>[^.]+)/$', calculatorItemDetails.as_view()),


]