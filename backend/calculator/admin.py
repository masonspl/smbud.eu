from django import forms
from django.contrib import admin
from calculator.models import *
# Register your models here.
from trumbowyg.widgets import TrumbowygWidget

admin.site.register(Project)
admin.site.register(Room)
