from django.db import models
import django.utils.timezone

class Project(models.Model):
    RENEW_TYPES = (
        ('150', 'Standard'),
        ('200', 'Kompleksowy'),
        ('350', 'Gruntowy'),
    )
    GENERAL_TYPES = (
        ('150', 'Standard'),
        ('200', 'Kompleksowy'),
        ('350', 'Gruntowy'),
    )
    TOP_TYPES = (
        ('150', 'Standard'),
    )
    OFFICE_TYPES = (
        ('150', 'Standard'),
        ('200', 'Kompleksowy'),
        ('350', 'Gruntowy'),
    )
    name = models.CharField(max_length=255)
    justrenew = models.CharField(max_length=255, choices=RENEW_TYPES, default='Standard')
    generalrenew = models.CharField(max_length=255, choices=GENERAL_TYPES, default='Standard')
    toprenew = models.CharField(max_length=255, choices=TOP_TYPES, default='Standard')
    officerenew = models.CharField(max_length=255, choices=OFFICE_TYPES, default='Standard')
    date_created = models.DateTimeField(default=django.utils.timezone.now)
    total = models.BooleanField(default=False)

class Room(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, default='0')
    name = models.CharField(max_length=255)
    date_created = models.DateField(default=django.utils.timezone.now)
    width = models.IntegerField(default='0')
    lenght = models.IntegerField(default='0')
    height = models.IntegerField(default='0')
    kitchen = models.BooleanField(default=False)
    bathroom = models.BooleanField(default=False)
    toilet = models.BooleanField(default=False)


